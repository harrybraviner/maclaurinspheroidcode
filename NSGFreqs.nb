(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     14858,        446]
NotebookOptionsPosition[     13670,        405]
NotebookOutlinePosition[     14084,        421]
CellTagsIndexPosition[     14041,        418]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Function definitions - Run first", "Section",
 CellChangeTimes->{{3.554955353201218*^9, 3.554955357809722*^9}, {
  3.6417329536860523`*^9, 3.6417329558619423`*^9}}],

Cell["\<\
The Q_l^m function defined in Lindblom and Ipser. This differs from the \
LegendreQ function defined by Mathematica.\
\>", "Text",
 CellChangeTimes->{{3.557047290604528*^9, 3.557047315221859*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"LIQ", "[", 
   RowBox[{"l_", ",", "m_", ",", "z_"}], "]"}], ":=", 
  RowBox[{
   RowBox[{"(", 
    RowBox[{
     RowBox[{"Sqrt", "[", "Pi", "]"}], "*", 
     RowBox[{"Gamma", "[", 
      RowBox[{"l", "+", "m", "+", "1"}], "]"}], "*", 
     RowBox[{"Exp", "[", 
      RowBox[{"I", "*", "m", "*", "Pi"}], "]"}], "*", 
     RowBox[{
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"z", "^", "2"}], "-", "1"}], ")"}], "^", 
       RowBox[{"(", 
        RowBox[{"m", "/", "2"}], ")"}]}], "/", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"2", "^", 
         RowBox[{"(", 
          RowBox[{"l", "+", "1"}], ")"}]}], "*", 
        RowBox[{"Gamma", "[", 
         RowBox[{"l", "+", "1"}], "]"}], "*", 
        RowBox[{"z", "^", 
         RowBox[{"(", 
          RowBox[{"l", "+", "m", "+", "1"}], ")"}]}]}], ")"}]}]}], ")"}], "*", 
   RowBox[{"Hypergeometric2F1", "[", 
    RowBox[{
     RowBox[{
      RowBox[{"(", 
       RowBox[{"l", "+", "m", "+", "1"}], ")"}], "/", "2"}], ",", 
     RowBox[{
      RowBox[{"(", 
       RowBox[{"l", "+", "m", "+", "2"}], ")"}], "/", "2"}], ",", 
     RowBox[{"l", "+", 
      RowBox[{"3", "/", "2"}]}], ",", 
     RowBox[{"1", "/", 
      RowBox[{"z", "^", "2"}]}]}], "]"}]}]}]], "Input",
 CellChangeTimes->{{3.557047275599313*^9, 3.55704728577151*^9}, {
  3.557047401189783*^9, 3.557047510384875*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"X0", "[", 
   RowBox[{"Z0_", ",", "k_"}], "]"}], " ", ":=", " ", 
  RowBox[{"If", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"k", "^", "2"}], "<", 
     RowBox[{"4", "*", 
      RowBox[{"(", 
       RowBox[{"1", "+", 
        RowBox[{"Z0", "^", "2"}]}], ")"}]}]}], ",", "\[IndentingNewLine]", 
    RowBox[{"Z0", "*", 
     RowBox[{
      RowBox[{"Abs", "[", "k", "]"}], "/", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{"4", "*", 
          RowBox[{"(", 
           RowBox[{"1", "+", 
            RowBox[{"Z0", "^", "2"}]}], ")"}]}], "-", 
         RowBox[{"k", "^", "2"}]}], ")"}], "^", "0.5"}]}]}], ",", 
    "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{"-", "I"}], "*", "Z0", "*", 
     RowBox[{
      RowBox[{"Abs", "[", "k", "]"}], "/", 
      RowBox[{
       RowBox[{"(", 
        RowBox[{
         RowBox[{
          RowBox[{"-", "4"}], "*", 
          RowBox[{"(", 
           RowBox[{"1", "+", 
            RowBox[{"Z0", "^", "2"}]}], ")"}]}], "+", 
         RowBox[{"k", "^", "2"}]}], ")"}], "^", "0.5"}]}]}]}], 
   "\[IndentingNewLine]", "]"}]}]], "Input",
 CellChangeTimes->{{3.554812588332118*^9, 3.554812589941296*^9}, {
  3.55481262088643*^9, 3.554812729601879*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"A", "[", 
   RowBox[{"Z0_", ",", "k_", ",", "l_", ",", "m_"}], "]"}], "  ", ":=", " ", 
  RowBox[{"If", "[", 
   RowBox[{
    RowBox[{
     RowBox[{"k", "^", "2"}], "<", 
     RowBox[{"4", "*", 
      RowBox[{"(", 
       RowBox[{"1", "+", 
        RowBox[{"Z0", "^", "2"}]}], ")"}]}]}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{
      FractionBox[
       RowBox[{
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Z0", "^", "2"}], "+", "1"}], ")"}], "*", 
        RowBox[{"Sign", "[", "k", "]"}]}], 
       RowBox[{
        RowBox[{"LegendreP", "[", 
         RowBox[{"l", ",", "m", ",", 
          RowBox[{"X0", "[", 
           RowBox[{"Z0", ",", "k"}], "]"}]}], "]"}], "*", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{"4", "*", 
            RowBox[{"(", 
             RowBox[{"1", "+", 
              RowBox[{"Z0", "^", "2"}]}], ")"}]}], "-", 
           RowBox[{"k", "^", "2"}]}], ")"}], "^", "0.5"}]}]], "*", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"LegendreP", "[", 
           RowBox[{"l", ",", "m", ",", "dummyZ"}], "]"}], ",", "dummyZ"}], 
         "]"}], "/.", 
        RowBox[{"dummyZ", "\[Rule]", " ", 
         RowBox[{"X0", "[", 
          RowBox[{"Z0", ",", "k"}], "]"}]}]}], ")"}]}], " ", "+", " ", 
     FractionBox[
      RowBox[{"2", "*", "m", "*", "Z0"}], 
      RowBox[{"4", "-", 
       RowBox[{"k", "^", "2"}]}]]}], ",", "\[IndentingNewLine]", 
    RowBox[{
     RowBox[{
      FractionBox[
       RowBox[{
        RowBox[{"-", "I"}], "*", 
        RowBox[{"(", 
         RowBox[{
          RowBox[{"Z0", "^", "2"}], "+", "1"}], ")"}], "*", 
        RowBox[{"Sign", "[", "k", "]"}]}], 
       RowBox[{
        RowBox[{"LegendreP", "[", 
         RowBox[{"l", ",", "m", ",", 
          RowBox[{"X0", "[", 
           RowBox[{"Z0", ",", "k"}], "]"}]}], "]"}], "*", 
        RowBox[{
         RowBox[{"(", 
          RowBox[{
           RowBox[{
            RowBox[{"-", "4"}], "*", 
            RowBox[{"(", 
             RowBox[{"1", "+", 
              RowBox[{"Z0", "^", "2"}]}], ")"}]}], "+", 
           RowBox[{"k", "^", "2"}]}], ")"}], "^", "0.5"}]}]], "*", 
      RowBox[{"(", 
       RowBox[{
        RowBox[{"D", "[", 
         RowBox[{
          RowBox[{"LegendreP", "[", 
           RowBox[{"l", ",", "m", ",", "dummyZ"}], "]"}], ",", "dummyZ"}], 
         "]"}], "/.", 
        RowBox[{"dummyZ", "\[Rule]", " ", 
         RowBox[{"X0", "[", 
          RowBox[{"Z0", ",", "k"}], "]"}]}]}], ")"}]}], " ", "+", " ", 
     FractionBox[
      RowBox[{"2", "*", "m", "*", "Z0"}], 
      RowBox[{"4", "-", 
       RowBox[{"k", "^", "2"}]}]]}]}], "\[IndentingNewLine]", 
   "]"}]}]], "Input",
 CellChangeTimes->{{3.554812912859555*^9, 3.554813134167353*^9}, {
  3.555912814142943*^9, 3.555912839472851*^9}, {3.555928130326799*^9, 
  3.555928134334529*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"F", "[", 
    RowBox[{"Z0_", ",", "k_", ",", "l_", ",", "m_"}], "]"}], " ", ":=", " ", 
   RowBox[{"1", "-", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"Z0", "/", "k"}], ")"}], "*", 
     RowBox[{"A", "[", 
      RowBox[{"Z0", ",", "k", ",", "l", ",", "m"}], "]"}]}]}]}], 
  ";"}]], "Input",
 CellChangeTimes->{{3.637321679737522*^9, 3.637321712674288*^9}, 
   3.637324101140429*^9, {3.637326062561409*^9, 3.637326066353078*^9}, {
   3.6373263687845173`*^9, 3.6373263728491898`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Z0toOmega", "[", "Z0_", "]"}], " ", "=", " ", 
   RowBox[{
    RowBox[{"Sqrt", "[", 
     RowBox[{"4", "/", "3"}], "]"}], "/", 
    RowBox[{"Sqrt", "[", 
     RowBox[{"1", "+", 
      RowBox[{"Z0", "^", "2"}]}], "]"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.637323369453558*^9, 3.63732337804344*^9}, {
  3.6373234621962023`*^9, 3.637323474348827*^9}, {3.637326089352078*^9, 
  3.637326090719625*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"etoZ0", "[", "e_", "]"}], " ", ":=", " ", 
   RowBox[{"Sqrt", "[", 
    RowBox[{
     RowBox[{"(", 
      RowBox[{"1", "-", 
       RowBox[{"e", "^", "2"}]}], ")"}], "/", 
     RowBox[{"e", "^", "2"}]}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.637326703223077*^9, 3.637326726302619*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Kappas", "[", 
    RowBox[{"e_", ",", "l_", ",", "m_"}], "]"}], ":=", "\[IndentingNewLine]", 
   
   RowBox[{"(", 
    RowBox[{"k", "/.", 
     RowBox[{"NSolve", "[", 
      RowBox[{
       RowBox[{"F", "[", 
        RowBox[{
         RowBox[{"etoZ0", "[", "e", "]"}], ",", "k", ",", "l", ",", "m"}], 
        "]"}], ",", "k", ",", 
       RowBox[{"WorkingPrecision", "\[Rule]", " ", "20"}]}], "]"}]}], ")"}]}],
   ";"}]], "Input",
 CellChangeTimes->{{3.637326663473188*^9, 3.637326699390748*^9}, {
  3.637326736095627*^9, 3.637326765823234*^9}, {3.637327179862955*^9, 
  3.6373271812608128`*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"Freqs", "[", 
    RowBox[{"e_", ",", "l_", ",", "m_"}], "]"}], ":=", "\[IndentingNewLine]", 
   
   RowBox[{
    RowBox[{"(", 
     RowBox[{"k", "/.", 
      RowBox[{"NSolve", "[", 
       RowBox[{
        RowBox[{"F", "[", 
         RowBox[{
          RowBox[{"etoZ0", "[", "e", "]"}], ",", "k", ",", "l", ",", "m"}], 
         "]"}], ",", "k", ",", 
        RowBox[{"WorkingPrecision", "\[Rule]", " ", "20"}]}], "]"}]}], ")"}], 
    "*", 
    RowBox[{"Z0toOmega", "[", 
     RowBox[{"etoZ0", "[", "e", "]"}], "]"}]}]}], ";"}]], "Input",
 CellChangeTimes->{{3.637326663473188*^9, 3.637326699390748*^9}, {
  3.637326736095627*^9, 3.637326763926409*^9}, {3.6373271850862293`*^9, 
  3.637327186285429*^9}}]
}, Closed]],

Cell[CellGroupData[{

Cell["Find where the roots are", "Section",
 CellChangeTimes->{{3.641732679318233*^9, 3.6417326905506973`*^9}, {
  3.641732842526021*^9, 3.6417328428374968`*^9}}],

Cell["\<\
Example: find the frequencies of the l=4 m=3 modes of the e=0.5 \
non-self-gravitating spheroid in the rotating frame in units of the dynamical \
frequency\
\>", "Text",
 CellChangeTimes->{{3.641732857478154*^9, 3.641732908838233*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Freqs", "[", 
  RowBox[{"0.5", ",", "4", ",", "3"}], "]"}]], "Input",
 CellChangeTimes->CompressedData["
1:eJwd0FtIEwAUBuA5w1k5c1quUYwmYkMrxId6MG/TLDecKw0kZZjhJW0sqm0x
KbTQLtbIyC4sNG8r9jBMZ4y2ycwagaJmqbQtJStZmw8yRUxGtf88HD7O4edw
OIJK5akqJoPBOBIq6NWK1QaeP+e+0qiF4SLTHRh8PkAmcT48gcnTD57CCX9H
J1RWj5lgrbXLDAMtfeTeTakNbmMH7XDFxn4HddYYcvUS2wm/zMrJjdtpk/Cj
+dAsZLODXylXxZiHF3gFv+Bcr8MHl3PPrMCSNjn5eEvDBvVnm8irvMGwlyGz
nw2RWpZhK1TLVNth48z0DrgWP0O+bdZz4epS7h7Ky0oSoVPymbyYFUyG+Y8Y
KdBmU6bBmvzLZOvh8nRo+SMnu9KteXDOESAnjx4XQ5FLQkZFXy+Fu7tvkh57
WS2sb5STOq+0A0Z6r5EJHeG9UGZmkT8qRk20r8ZJPjypscDgv7ukxNo+QnfX
/x2F3xWcT1DBTCALYkpnodlVRvJvnV6ABx1DZEtq5BL9pyWKdIhNfphx7jVZ
GdG6DiuKdKRRv8l4FbJIcCIMRvTEsmB2Jpd0ZWZzoXsqj5wa5AuhyC4ghZa6
DJgkmiCv7HLlwALVN3KMGV0EAzIO+WKnuxz6fs+ThePN52G7UayAmnWNGsYd
ayD1fOENWJx6gMzvK9bBZJWlDcaWxunh2mI8WT5j6IZ1ins9MDMxywj5/SIy
xT3cD4PLI6Sn6ecbaJZ6yep9Pjt8P1A4DPePexbZCaG5ZoH8D1cbsDY=
  "]],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"NSolve", "::", "ratnz"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"NSolve was unable to solve the system with inexact \
coefficients. The answer was obtained by solving a corresponding exact system \
and numericizing the result. \\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", \
ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/NSolve\\\", ButtonNote -> \
\\\"NSolve::ratnz\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{3.641732960667221*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", "2.595646418485724`"}], ",", 
   RowBox[{"-", "1.154700538379252`"}], ",", 
   RowBox[{"-", "0.334149541099222`"}], ",", "1.775095421205694`"}], 
  "}"}]], "Output",
 CellChangeTimes->{{3.6417329438867207`*^9, 3.6417329606680927`*^9}}]
}, Open  ]],

Cell["\<\
Example: find the frequencies of the l=4 m=3 modes of the e=0.5 \
non-self-gravitating spheroid in the rotating frame in units of the spheroid \
angular frequency\
\>", "Text",
 CellChangeTimes->{{3.641732857478154*^9, 3.641732933333487*^9}}],

Cell[CellGroupData[{

Cell[BoxData[
 RowBox[{"Kappas", "[", 
  RowBox[{"0.5", ",", "4", ",", "4"}], "]"}]], "Input",
 CellChangeTimes->CompressedData["
1:eJwdzmtI0wEUBfDNwJwNmxlUI2S6MHUkobUepmyighpq0zIbM80e21gZCi6l
UGosjZQeVupqRo4Z08xIWthCsjnnWkvNJmgPCTXib1akQhLazv1w+MGBezlh
x0pkJ/xYLNYuX2Dw143nzJsYqTNyuRJOVQhqoMsuJN2lrKuwtGIV+Wd6vgmG
O+XNMPXugRa4WPvRBAP31nVAY23KI2g5mtANh0wScrmqtQfKfnBf0Z1p1AFj
BKIBOLmN8UBVkfcDTC6yjMOMFtcnOLfTbxrGunO/wYKuPAZGeNWkrnjHT3gt
O4Gs/P12ATbsGyZ5l20rkJXTS07ddga0+bRp7WugUpoaAj2bM8je9td8aP7X
T6aNRQlhgbGd3C8eiobim6Pkm1PcWDjXFkQGSv3j4ZdsDrnADk6GheVnyerd
5nQ47rCQYYkRh6GkNopcZ+QqoUzDIwfZrnuQp/I3Qr0+wwRFDVlk3tOAx3DE
ySW3vB97DnWCCXI2n98H3Q1Jdtg3pxymfX0aUm23eGFwfSfptXImqdeUkbq0
vBm4tVVOdmsXGTj1eYm0NnoWaZ96hHxpaGY/9JkZNEHm/M1dDY9HHCF5tlsb
4PxBA6m8czISuhgV6dDxE2F1QiaZ3rQ2CfbPhJDlkvwsGNqsIK9bQhWw43w4
+WSWo4bf6/SnYZfyWTnstPeQ0YMlF+HK+jLS/OJdPVwS+t2AhcVVBiiPu0TG
/BI9gFcYMXloT6MF1iwbSMUZbRe8f6GSHIjfbqV/KXHkfzC3s0A=
  "]],

Cell[BoxData[
 RowBox[{
  StyleBox[
   RowBox[{"NSolve", "::", "ratnz"}], "MessageName"], 
  RowBox[{
  ":", " "}], "\<\"NSolve was unable to solve the system with inexact \
coefficients. The answer was obtained by solving a corresponding exact system \
and numericizing the result. \\!\\(\\*ButtonBox[\\\"\[RightSkeleton]\\\", \
ButtonStyle->\\\"Link\\\", ButtonFrame->None, \
ButtonData:>\\\"paclet:ref/NSolve\\\", ButtonNote -> \
\\\"NSolve::ratnz\\\"]\\)\"\>"}]], "Message", "MSG",
 CellChangeTimes->{3.641732964840489*^9}],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"-", "4.605551275463989`"}], ",", 
   RowBox[{"-", "2.`"}], ",", "2.60555127546399`"}], "}"}]], "Output",
 CellChangeTimes->{3.6417329648413363`*^9}]
}, Open  ]],

Cell["\<\
(Note: Ignore any values at \[Kappa]=+2,-2,0, since the theory is not valid \
at these values\
\>", "Text",
 CellChangeTimes->{{3.64173296965337*^9, 3.6417329988537073`*^9}}]
}, Open  ]]
},
WindowSize->{1302, 990},
WindowMargins->{{308, Automatic}, {Automatic, 58}},
PrivateNotebookOptions->{"VersionedStylesheet"->{"Default.nb"[8.] -> False}},
FrontEndVersion->"10.0 for Linux x86 (64-bit) (June 27, 2014)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 170, 2, 65, "Section"],
Cell[752, 26, 206, 4, 31, "Text"],
Cell[961, 32, 1399, 42, 55, "Input"],
Cell[2363, 76, 1248, 39, 99, "Input"],
Cell[3614, 117, 2936, 87, 177, "Input"],
Cell[6553, 206, 534, 14, 32, "Input"],
Cell[7090, 222, 451, 12, 32, "Input"],
Cell[7544, 236, 342, 10, 32, "Input"],
Cell[7889, 248, 644, 18, 55, "Input"],
Cell[8536, 268, 751, 21, 55, "Input"]
}, Closed]],
Cell[CellGroupData[{
Cell[9324, 294, 162, 2, 51, "Section"],
Cell[9489, 298, 245, 5, 31, "Text"],
Cell[CellGroupData[{
Cell[9759, 307, 925, 17, 32, "Input"],
Cell[10687, 326, 527, 11, 23, "Message"],
Cell[11217, 339, 291, 7, 32, "Output"]
}, Open  ]],
Cell[11523, 349, 252, 5, 31, "Text"],
Cell[CellGroupData[{
Cell[11800, 358, 922, 17, 32, "Input"],
Cell[12725, 377, 527, 11, 23, "Message"],
Cell[13255, 390, 200, 5, 32, "Output"]
}, Open  ]],
Cell[13470, 398, 184, 4, 31, "Text"]
}, Open  ]]
}
]
*)

(* End of internal cache information *)
