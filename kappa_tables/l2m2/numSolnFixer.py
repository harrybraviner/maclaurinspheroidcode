#! /usr/bin/python

from math import asin, sqrt

import sys

if len(sys.argv)!= 4:
	print "Correct usage is:\nfreq2kappa\t[filename1]\t[filename2]\tnumber of solutions\nwhere [filename1] consists of lines of the format: eccentricity  freq1  freq2... and [filename2] is the file to which the output will be written\n"
	sys.exit()

def Omega(e):
	print e
	return sqrt(2*((3-2*e**2)*sqrt(1-e**2)*asin(e)/e - 3*(1-e**2))/(e**2))

f1 = open(sys.argv[1])

f2 = open(sys.argv[2], 'w');

for line in f1:
	outline = [];
	if line.strip()[0] == '#':
		continue
	else:
		try:
			if len(line.strip().split()[1:]) != int(sys.argv[3]):
				print "Removed line:", line
				continue
			else:
				outline = line
				f2.write(outline)
		except:
			continue
