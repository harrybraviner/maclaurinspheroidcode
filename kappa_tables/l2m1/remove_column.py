#! /usr/bin/python

import sys

if len(sys.argv) != 4:
	print "Incorrect usage. Correct usage is:"
	print "./reomve_column [n] [input_file] [output_file]"
	print "where column n is to be removed."

input_file = open(sys.argv[2], 'r')
output_file = open(sys.argv[3], 'w')

n_remove = int(sys.argv[1])

for line in input_file:
	if line.strip().split():	# Check it's not a blank line
		if line.strip().split()[0][0] != '#':	# Check it's not a comment line
			output_file.write('\t'.join(line.strip().split()[0:(n_remove-1)] + line.strip().split()[n_remove:])+'\n')
		else:
			output_file.write(line)
