#! /usr/bin/python

import sys

if len(sys.argv) != 4:
	print "Incorrect usage. Correct usage is:"
	print "./add_zero_column.py [input_file] [output_file] [n]"
	print "where input_file is to have a column of zeros added at position n."
	print "Columns are indexed from 1."

input_file = open(sys.argv[1], 'r')
output_file = open(sys.argv[2], 'w')

n_zero_col = int(sys.argv[3])

for line in input_file:
	if line.strip().split():	# Check it's not a blank line
		if line.strip().split()[0][0] != '#':	# Check it's not a comment line
			output_file.write('\t'.join(line.strip().split()[0:(n_zero_col-1)] + ['0.00000'] + line.strip().split()[(n_zero_col-1):])+'\n')
		else:
			output_file.write(line)
